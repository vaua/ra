from selenium.webdriver.common.by import By
from datetime import datetime, timedelta, date


def nights(count=3):
    today = str(date.today())
    end_day = date.fromisoformat(today) + timedelta(days=count)
    return By.CSS_SELECTOR, f'.bui-calendar__date[data-date="{end_day}"]'


class BookingStartPage:
    search = (By.ID, 'ss')
    dates = (By.CSS_SELECTOR, 'div.xp__dates.xp__group')
    search_button = (By.CSS_SELECTOR, 'button.sb-searchbox__button')
    today = (By.CSS_SELECTOR, '.bui-calendar__date--today')
    checkout = nights()


class BookingSelectHotel:
    check_room = (By.CSS_SELECTOR, 'a.sr_cta_button')


class BookingBookRoom:
    select_rooms = (By.CSS_SELECTOR, 'select.hprt-nos-select')
    book = (By.CSS_SELECTOR, r'.hprt-reservation-cta > button')


class Altruisto:
    popup = (By.CSS_SELECTOR, '.altruisto-notification')
    close_popup = (By.CSS_SELECTOR, 'button.altruisto-notification__close')
    activate_button = (By.CSS_SELECTOR, '.altruisto-notification__button-wrapper')
    notification_text = (By.CSS_SELECTOR, '.altruisto-notification__text')

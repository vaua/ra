from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from allure import step


class BaseElement:
    def __init__(self, driver, locator):
        self.driver = driver
        self.locator = locator

        self.web_element = None
        self.find()

    def find(self):
        element = WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(locator=self.locator)
        )
        self.web_element = element
        return None

    @step("Input text {txt}")
    def input_text(self, txt):
        self.web_element.send_keys(txt)
        return None

    @step("Click on element")
    def click(self):
        element = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locator=self.locator)
        )
        element.click()
        return None

    def attribute(self, attr_name):
        attribute = self.web_element.get_attribute(attr_name)
        return attribute

    @step("Select {num} from dropdown")
    def select(self, num):
        sel = Select(self.web_element)
        sel.select_by_index(num)
        return None

    @property
    def text(self):
        text = self.web_element.text
        return text

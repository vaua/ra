from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from allure import step

class CommonSteps:
    def __init__(self, driver):
        self.driver: webdriver.Chrome = driver
    @step
    def close_extra_tabs(self):
        for window in self.driver.window_handles:
            self.driver.switch_to.window(window)
            if self.driver.title.startswith('altr'):
                self.driver.close()


class CommonAsserts:
    def __init__(self, driver):
        self.driver: webdriver.Chrome = driver

    @step
    def page_contain_element(self, locator):
        assert WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(locator=locator)), 'Element  is absent on the page'
    @step
    def element_contain_text(self, locator, text):
        element = WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(locator=locator))
        assert text in element.text

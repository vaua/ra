#  Altruisto extension testing

Framework for testing Altruisto extension in Chrome browser.

## Prerequairments

Windows, Ubuntu or macOS with installed python v3.9, git, Chrome browser, chromewebdriver

Optional:

Docker with installed Selenoid [Selenium in Docker](https://aerokube.com/selenoid/).

[Download](https://github.com/aerokube/cm/releases) file for your OS

Windows:
```bash
./cm_windows_amd64.exe selenoid start --vnc
./cm_windows_amd64.exe selenoid-ui start
```
Linux:
```bash
sudo chmod +x ./cm_linux_amd64
sudo ./cm_linux_amd64 selenoid start --vnc
sudo ./cm_linux_amd64 selenoid-ui start
```
Allure
```bash

```


## Installation

Clone project

```bash
pip install -r requirements.txt
```

## Usage
Run test
```bash
python -m pytest
```
Run test with creating report 
```bash
python -m pytest --alluredir=$PATH_TO_DIR_REPORT
```

Build Docker image:
```bash
docker build -t ra .
```

Run test in docker without creating report 
```bash
docker run -ti ra pytest
```

Run test in docker with creating report 
Change docker_ip in conftest.py on IP of your SelenoID server

```bash
docker run -ti -v $(FULL_PATH_FOR_REPORT_FOLDER):/usr/src/ra/report ra pytest --alluredir=report
```
Veiw report:

Install allure
```bash
allure serve $(PATH_TO_REPORT_FOLDER)
```

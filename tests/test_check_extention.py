from elements.pages import BookingStartPage, BookingSelectHotel, BookingBookRoom, Altruisto


def test_app(app):
    app.common.close_extra_tabs()
    app.driver.refresh()
    # Activating Altruisto extention
    app.check.page_contain_element(Altruisto.popup)
    app.element(Altruisto.activate_button).click()
    app.check.element_contain_text(Altruisto.notification_text,
                                   'You are now collecting money for charities with this website')
    app.element(Altruisto.close_popup).click()
    # Select city and dates for booking
    app.element(BookingStartPage.search).input_text('Barcelona')
    app.element(BookingStartPage.dates).click()
    app.element(BookingStartPage.today).click()
    app.element(BookingStartPage.checkout).click()
    app.element(BookingStartPage.search_button).click()
    app.element(BookingSelectHotel.check_room).click()
    # Switch to new tab with results
    app.driver.switch_to.window(app.driver.window_handles[-1])
    app.element(BookingBookRoom.select_rooms).select(1)
    app.element(BookingBookRoom.book).click()
    app.check.element_contain_text(Altruisto.notification_text,
                                   'You are now collecting money for charities with this website')
    app.element(Altruisto.close_popup).click()

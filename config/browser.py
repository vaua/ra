from elements.common import CommonSteps, CommonAsserts
from elements.elements import BaseElement
from selenium import webdriver


class Browser:
    def __init__(self, driver):
        self.driver: webdriver.Chrome = driver
        self.common = CommonSteps(self.driver)
        self.check = CommonAsserts(self.driver)

    def element(self, locator):
        element = BaseElement(self.driver, locator)
        return element

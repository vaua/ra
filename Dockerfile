FROM python:3.9

USER root

RUN wget https://bootstrap.pypa.io/get-pip.py
RUN python3 get-pip.py
ENV RUNNING_IN_DOCKER_CONTAINER Yes

RUN apt update -y && apt install git
RUN mkdir -p /usr/src/ra
WORKDIR /usr/src/ra/
RUN git clone https://gitlab.com/vaua/ra.git /usr/src/ra/

RUN python3 -m pip install -r /usr/src/ra/requirements.txt

CMD ["/bin/bash"]

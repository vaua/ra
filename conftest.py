import pytest
import allure
import os
from selenium import webdriver
from config.browser import Browser
from selenium.webdriver.chrome.options import Options

docker_ip = '172.16.172.16'

def pytest_runtest_makereport(item, call) -> None:
    dir(item)
    if call.when == "call":
        if call.excinfo is not None and "app" in item.funcargs:
            page = item.funcargs["app"]
            allure.attach(page.driver.get_screenshot_as_png(), name=item.name,
                          attachment_type=allure.attachment_type.PNG)


def select_where_run():
    in_docker = os.environ.get('RUNNING_IN_DOCKER_CONTAINER', False)
    options = Options()
    options.add_extension('extentions/djennkbadhfcmhlbejdidgmdgnacbcmi_v.3.0.3.crx')

    if in_docker:
        capabilities = {
            "browserName": "chrome",
            "browserVersion": "89.0",
            "selenoid:options": {
                "enableVNC": True,
                "enableVideo": False}
        }
        driver = webdriver.Remote(
            command_executor=f"http://{docker_ip}:4444/wd/hub",
            desired_capabilities=capabilities, options=options)
        return driver
    else:
        driver = webdriver.Chrome(options=options)
        return driver

@pytest.fixture(scope='function')
def app():

    driver = select_where_run()
    driver.set_window_size(1920, 1080)
    driver.get('https://booking.com')
    driver = Browser(driver)
    yield driver
    driver.driver.quit()
